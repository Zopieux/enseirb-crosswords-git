#!/usr/bin/env python

import os
from lxml import html
from pprint import pprint
import operator
import itertools
try:
    from functools import reduce
except ImportError:
    pass

DICO = '../data/ods5.words'
SEARCH = 'grids'


def find_words(chars):
    tmp = []
    for l in chars:
        if l is None and tmp != []:
            yield ''.join(tmp)
            tmp = []
        elif l is not None:
            tmp.append(l)

    # fin de boucle
    if tmp != []:
        yield ''.join(tmp)


def main():
    with open(DICO, 'r') as f:
        WORDS = list(filter(None, f.read().split('\n')))

    for root, dirs, fnames in os.walk(SEARCH):
        for fname in fnames:
            if not fname.endswith('.html'): continue

            with open(os.path.join(root, fname), 'r') as f:
                try:
                    source = html.parse(f)
                except AssertionError:
                    continue

            lines = []
            cols = []
            try:
                for col in source.findall('//tbody/tr'):
                    tmpline = []
                    for row in col.findall('td'):
                        tmpline.append(None if row.get('class') else row.text)
                    lines.append(tmpline)
            except AssertionError:
                continue

            width = len(lines[0])
            for i in range(width):
                cols.append([line[i] for line in lines])

            words = list(reduce(operator.add, [list(find_words(_)) for _ in list(lines) + list(cols)], []))

            longwords = list(filter(lambda _: len(_) > 2, words))
            longwords_set = set(longwords)  # uniquify words

            no_duplicates = len(longwords) == len(longwords_set)
            all_exists = all(map(lambda _: _ in WORDS, longwords))

            print("%s  %2d WORDS  NO-DUPLICATES %s  ALL-EXIST %s  OK %s" % (
                fname.ljust(34),
                len(words),
                'YES' if no_duplicates else 'NO ',
                'YES' if all_exists else 'NO ',
                'YES' if no_duplicates and all_exists else 'NO',
            ))

if __name__ == '__main__':
    main()
