#!/usr/bin/env python

import glob
from pprint import pprint
from collections import defaultdict

def apply(dic, f):
    return {k: f(v) for k, v in dic.items()}

def convert(fn):
    with open(fn, 'r') as f:
        data = filter(bool, f.read().split('\n'))

    out = {}
    for line in data:
        try:
            a, b, c = line.split()
        except ValueError:
            continue
        out[(int(a), int(b))] = float(c)

    return out

def main(path, func):

    out = defaultdict(lambda: [])

    for fname in glob.glob(path):
        d = convert(fname)
        for k, v in d.items():
            out[k].append(v)

    out = dict(out)
    applied = apply(out, func)

    print('\n'.join(sorted('%d %d %f' % (k[0], k[1], v) for k, v in applied.items())))

if __name__ == '__main__':
    import sys
    main(sys.argv[1], eval(sys.argv[2]))
