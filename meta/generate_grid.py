#!/usr/bin/python

import sys
import random
import itertools
import math

BLACK = '#'
BLANK = ' '

def w(wat):
	sys.stdout.write(wat)

def generate(n, m, blackprop):
	if not 0 <= blackprop <= 100:
		raise ValueError

	blackprop /= 100.
	allcells = range(1, n * m +1)
	nb_black = int(math.floor(n * m * blackprop))
	blacks = random.sample(allcells, nb_black)

	w('%d\n%d\n' % (n, m))
	for i in xrange(1, n * m + 1):
		w(BLACK if i in blacks else BLANK)
		if i > 1 and i % m == 0:
			w('\n')

	w('\n')
	# sys.stdout.flush()

if __name__ == '__main__':
	generate(*map(int, sys.argv[1:4]))
