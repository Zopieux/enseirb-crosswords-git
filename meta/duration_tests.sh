#!/bin/bash

PERCENT=$1
HOSTNAME=`hostname`
PGMROOT=/net/i/amacabies/projets/s5/crosswords
RESROOT=/net/i/amacabies/tmp/cwresults
TMPFILE=`mktemp`
RESFILE=$RESROOT/$PERCENT/${HOSTNAME}.results
SUCCFILE=$RESROOT/hasfinished

# create result dir for this percent
mkdir -p $RESROOT/$PERCENT/grids
# empty res
echo -n "">$RESFILE
touch $SUCCFILE

# we kill the shit after too long
(sleep 3600; kill -9 $$)&

for n in {1..9}
do
	for m in {1..9}
	do

		mkdir -p generated-$2
		python $PGMROOT/meta/generate_grid.py $n $m $PERCENT>$TMPFILE
		echo "$n $m" `/usr/bin/time -f "%e" $PGMROOT/crosswords -f html -o $RESROOT/$PERCENT/grids/${n}x${m}.${HOSTNAME}.html $TMPFILE $PGMROOT/data/ods5.words 2>&1`>>$RESFILE

	done
done

rm $TMPFILE
echo $HOSTNAME>>$SUCCFILE

