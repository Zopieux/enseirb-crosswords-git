\label{probleme-rendu}
Une fois que l'algorithme de génération est terminé, la grille réside en
mémoire dans la structure C correspondante (\code{struct Grid}).
Pour qu'un utilisateur humain puisse la lire, il est nécessaire de \emph{serializer} cette
structure en un format qui peut être enregistré sur le disque.

Comme expliqué en \ref{composantes}, le programme permet de produire au choix trois
formats de rendu des grilles. Cette génération n'est pas complexe, il s'agit
simplement de \emph{génération de code}: nous écrivons, via un programme écrit en C,
du code dans un autre langage (en l'occurrence HTML et \LaTeX{}). La procédure se
résume donc ainsi:

\begin{algorithm}[H]
	\caption{Rendu de la grille dans un certain format}
	\Entree{grille: $Grille$, fichier: $Fichier$ (flux)}
	% \Sortie{}
	\'Ecrire($fichier$, Préambule_global($grille$))\\
	\Pour{$ligne$ \textbf{de} 1 \KwA $grille.nombre\_lignes$}{
		\'Ecrire($fichier$, Préambule_pour_ligne($ligne$))\\
		\Pour{$colonne$ \textbf{de} 1 \KwA $grille.nombre\_colonnes$} {
			\'Ecrire($fichier$, Préambule_pour_colonne($colonne$))\\
			\'Ecrire($fichier$, Format_cellule($ligne$, $colonne$))\\
			\'Ecrire($fichier$, \'Epilogue_pour_colonne($colonne$))\\
		}
		\'Ecrire($fichier$, \'Epilogue_pour_ligne($ligne$))\\
	}
	\'Ecrire($fichier$, \'Epilogue_global($grille$))
\end{algorithm}

On suppose gérés, en dehors de cette fonction, l'ouverture et la fermeture
du fichier \--- ainsi que la gestion des potentielles erreurs qui, on le
sait, peuvent être nombreuses quand on travaille sur les fichiers.

\subsection*{Format brut (ou ASCII)}
Le format brut est le format \og canonique \fg{} de représentation d'une grille.
On retranscrit les cellules de la grille dans un fichier texte, comme si on
écrivait la grille avec un stylo sur du papier. On choisira arbitrairement de
représenter les cases noires par un caractère spécial, qui bien-sûr n'appartient
pas à l'alphabet, ici le dièse.
La figure \ref{fig:raw} montre le résultat pour une grille d'exemple.

\begin{figure}[h]
\begin{wideverbatim}
DEREEL
ERABLE
RENAUX
A#AU#I
TILDEE
ELEIS#
\end{wideverbatim}
    \caption{Exemple de format brut}
    \label{fig:raw}
\end{figure}
Le fait que les caractères numériques soient plus hauts qu'ils ne sont larges rend
plutôt ardue la lecture des mots en colonnes \--- d'autant plus que ce n'est
pas le sens de lecture habituel pour les langues indo-européennes, qui se fait
de gauche à droite.

\subsection*{Format HTML}
Le HTML se génère facilement: il s'agit d'un dialecte dérivé du XML et notre
format est ici volontairement dénudé de mise en forme évoluée. On se
contente de créer un tableau (\code{<table>}) en distinguant les cases noires
par un attribut spécial, \code{class="bs"}, qui colore en l'occurrence la case
avec un fond noir. La figure \ref{fig:htmlcode} en annexe montre le code HTML pour une grille d'exemple
et son rendu sur la figure \ref{fig:htmlrender}.

\begin{figure}[h!]
    \includegraphics[width=\textwidth]{sc-html.png}
    \caption{Rendu du format HTML dans un navigateur}
    \label{fig:htmlrender}
\end{figure}
On notera qu'il est bien plus agréable de lire cette grille dans un navigateur
par rapport au format brut pour lequel les cellules ne font pas la même taille
et ne possèdent pas de contour.

\subsection*{Format \LaTeX}
Le \LaTeX{} a l'avantage d'être transformable lui-même en d'autres formats,
comme le fameux \emph{Portable Document Format}, et il est particulièrement
adapté à l'impression. Sa génération depuis le C s'avère toutefois rebutante
car \LaTeX{} recourt au \emph{backslash} pour préfixer ses commandes alors
que ce caractère sert justement d'échappement en C. Il faut donc le doubler
dans le code C, ce qui rend les sources peu digestes!
La figure \ref{fig:latex} en annexe montre le code \LaTeX pour une grille d'exemple
et la figure \ref{fig:latexrender} est son rendu.

\begin{figure}[h!]
    \input{rapport-example-latex}
    \caption{Rendu du format \LaTeX\\Essayez de zoomer depuis votre lecteur PDF!}
    \label{fig:latexrender}
\end{figure}
