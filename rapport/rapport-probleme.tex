\label{composantes}
On distingue trois grandes composantes, que l'on peut traiter indépendemment
ou presque: le chargement d'un dictionnaire dans la mémoire et son utilisation
de manière efficace, la génération de la grille et enfin son affichage (ou rendu).
Cette dernière partie étant triviale, on l'abordera avant la génération à
proprement parler.

\subsection{Le chargement d'un dictionnaire}
Pour générer la grille, il est nécessaire de choisir les mots dans un
\emph{dictionnaire}, c'est-à-dire une liste exhaustives des mots qui peuvent
apparaître dans les mots-croisés produits.
Bien-entendu, le dictionnaire sera de taille très importante si l'on
souhaite y intégrer la plupart des mots d'une langue donnée. Pour se donner une
idée, il existe plus de 200~000 mots dans la langue française et plus de 600~000
dans la langue anglaise, en comptant les différentes formes d'une même entité
lexicale. La difficulté résidera donc clairement dans l'utilisation d'une
structure de donnée efficace pour charger et utiliser une telle quantité de données
(plusieurs méga-octets pour l'Officiel du Scrabble, par exemple).

Le dictionnaire $\mathcal{D}$ contiendra uniquement des mots sur un alphabet $\Sigma$ qui sera
imposé par l'implantation en langage C (cf. partie \ref{ss:generation}).
Pour des raisons de simplicité et notamment pour pallier les problèmes d'encodage
des caractères spéciaux\footnote{Caractères hors ASCII.} en C, l'alphabet $\Sigma$
sera réduit aux vingt-six lettres de l'alphabet latin:
\begin{equation*}\Sigma = \left\{A, \ldots, Z\right\}\end{equation*}

Il est également important de prendre en considération les opérations qui seront
nécessaires d'effectuer sur ce dictionnaire. Dans l'implantation de notre
algorithme, par exemple, nous avons uniquement besoin de rechercher l'ensemble
$P_\omega$ des mots qui sont préfixés par une suite $\omega$ de lettres donnée:
\begin{equation*}P_\omega = \left\{m \in \mathcal{D} | \exists u \in \Sigma^*, m = \omega \cdot u\right\}\end{equation*}

L'idée d'utiliser un tableau semble immédiate, mais pour que cette recherche soit
efficace, on comprend bien qu'il faut le tableau soit trié dans l'ordre lexicographique.
On cherche alors en
temps linéaire la première apparition de la suite $\omega$ et l'on s'arrête quand
le début des mots parcourus n'est plus $\omega$.\\
Toutefois, cette implantation est trop simple: plus les préfixes cherchés sont loin
dans l'alphabet, plus la recherche devient coûteuse. Notre algorithme a par
ailleurs besoin de connaître rapidement le nombre de mots préfixés par $\omega$.
Pour pallier ce problème de performance, nous nous sommes finalement tournés vers
une structure d'arbre lexicographique où chaque n\oe{}ud contient le nombre de
mots préfixés par le chemin jusqu'à ce n\oe{}ud et dont les fils (au nombre de $\left|\Sigma\right|$)
sont les lettres suivantes. Voyez l'exemple figure \ref{fig:dico-tree}.

\begin{equation*}\mathcal{D}_1 = \left\{AA\footnotemark, AS, ABAT, ABEE, BAR, BAL, BALS\right\}\end{equation*}
\footnotetext{Un aa est un type de coulée de lave ou une rivière.}
\begin{figure}[h]
	\caption{Illustration de l'arbre représentant le dictionnaire $\mathcal{D}_1$\\
	Les lettres qui n'ont aucun fils ne sont pas représentées par soucis de concision.
	}
	\label{fig:dico-tree}
	\centering
\begin{tikzpicture}[level distance=10mm]
	\tikzstyle{every node}=[circle,inner sep=1pt]
	\tikzstyle{level 1}=[sibling distance=30mm]
	\tikzstyle{level 2}=[sibling distance=15mm]
	\tikzstyle{level 3}=[sibling distance=10mm]
	\tikzstyle{level 4}=[sibling distance=10mm]

  \node {\#}
	child { node {$\underset{4}{A}$}
		child { node {$\underset{1}{A^\star}$}
			% child { node {$\underset{1}{\bullet}$}}
		}
		child { node {$\underset{2}{B}$}
			child { node {$\underset{1}{A}$}
				child { node {$\underset{1}{T^\star}$}
					%child { node {$\underset{1}{\bullet}$}}
				}
			}
			child { node {$\underset{1}{E}$}
				child { node {$\underset{1}{E^\star}$}
					%child { node {$\underset{1}{\bullet}$}}
				}
			}
		}
		child { node {$\underset{1}{S^\star}$}
			%child { node {$\underset{1}{\bullet}$}}
		}
	}
	child { node {$\underset{3}{B}$}
		child { node {$\underset{3}{A}$}
			child { node {$\underset{1}{R^\star}$}
				% child { node {$\underset{1}{\bullet}$}}
			}
			child { node {$\underset{2}{L^\star}$}
				% child { node {$\underset{1}{\bullet}$}}
				child { node {$\underset{1}{S^\star}$}
					% child { node {$\underset{1}{\bullet}$}}
				}
			}
		}
	}
	;
\end{tikzpicture}\end{figure}

Cette structure se traduit formellement par:
\begin{verbatim}DicoNoeud
  |                 (entier) Nombre de mots dans les fils
  | (tableau de DicoNoeud`s) Fils de ce noeud
\end{verbatim}


\subsection{L'export (ou rendu) des grilles}
Quand l'algorithme a généré une grille, il semble particulièrement intéressant de pouvoir
récupérer le résultat! Toutefois, il existe une extraordinaire diversité de formats pour
stocker ce genre de chose. C'est la raison pour laquelle nous avons choisi d'implanter,
au sein du programme, trois formats de \og rendu \fg{} (ou d'export) des grilles générées:
\emph{brut} (ASCII), \emph{HTML} et \emph{\LaTeX}. Ce choix a été motivé par leur relative
simplicité et leur facilité d'utilisation:
\begin{itemize}
\item Le format brut est simplissime et peut facilement être compris par d'autres
programmes informatiques qui pourront le transformer, si besoin, dans des formats plus évolués.
\item Le format HTML est facile à écrire et presque toutes les machines dotées d'un
affichage graphique possèdent un navigateur.
\item Le format \LaTeX{} est plus lourd à écrire \--- surtout en C \--- mais permet de
créer, à l'aide d'un outil approprié comme \code{pdflatex}, un fichier PDF qui est l'un
des formats les plus répandus dans l'univers numérique.

\end{itemize}
\vspace{1em}
Nous donnons des exemples de fichiers générés en partie \ref{probleme-rendu}.

\subsection{La génération à proprement parler}

Le but du projet est la génération d'une grille de mots croisés. Concrètement, un mot
est ici défini par une suite finie de lettres qui peut être:
\begin{itemize}
\item préfixée par une case noire;
\item \emph{et/ou} suffixée par une case noire;
\item \emph{et/ou} commencer au bord de la grille;
\item \emph{et/ou} finir au bord de la grille.
\end{itemize}

\vspace{1em}
Cette définition peut paraître triviale mais l'expliciter clairement nous a permis
de bien poser les conditions limites de l'algorithme.
On peut dès lors définir une grille
de mots croisés comme une matrice de caractères vérifiant les contraintes suivantes:
\begin{itemize}
  \item chaque case est une lettre de l'alphabet ou une case noire;
  \item les mots en ligne (respectivement en colonne) appartiennent au dictionnaire;
  \item un mot (de plus de deux lettres) ne peut pas apparaître plus d'une fois sur la grille.
\end{itemize}

\vspace{1em}
Ces définitions étant posées, il sera facile d'implanter un algorithme qui les respecte.
Nous détaillons en partie \ref{probleme-algo} la méthode de génération que nous avons utilisé.

\label{ss:generation}
