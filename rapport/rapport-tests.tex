\label{tests}
\subsection*{Méthode: \emph{benchmarks} distribués}

Pour tester la correction et les performances de notre programme, nous avons
réalisé une réalisé des séries de tests sur des grilles de taille variée:
dimensions $N \times M$ avec $N,M \in \llbracket 1, 9 \rrbracket^2$.

Pour obtenir rapidement une quantité significatives de tests, nous avons écrit
un ensemble de scripts \emph{bash} permettant de distribuer les tests sur le
parc de machines de l'ENSEIRB-MATMECA pendant le week-end, où celles-ci ne sont
pratiquement pas utilisées. Un premier script va s'identifier en SSH sur les machines
disponibles et, si le login fonctionne, va démarrer un second script\footnote{Source
disponible dans \code{meta/duration_tests.sh}.} qui lancera la série de tests.
Les résultats sont récupérés sur le compte personnel d'un des membres du trinôme
pour permettre l'aggrégation et l'analyse des données.

Cette méthode a permis de lancer les tests sur environ 99 machines, générant
plus de 6500 grilles par série. Nous n'avons pas obtenu les $9\times 9 \times 99 \approx 8000$
grilles théoriquement faisables car, pour éviter de surcharger inutilement les machines,
le script s'arrêtait automatiquement après \emph{une heure} de calcul, temps au bout
duquel certaines machines ne sont pas arrivées au bout des calculs (grilles de grande
taille).


Les résultats sont présentés sous forme de graphique à trois dimensions. Nous
obtenons alors un graphique par pourcentage de cases noires dans la grille.
	   Le placement des cases noires est aléatoire et donc pour un même pourcentage,
les résultats peuvent varier, c'est pourquoi nous lançons plusieurs séries pour chaque
pourcentage. Par ailleurs, pour une configuration fixée de cases noires, notre algorithme
est déterministe, et donc plus précisément pour une absence totale de cases noires, seule
une série de test est nécessaire.

	L'interprétation des graphiques sera bien sûr à mettre en relation avec la complexité
de l'algorithme que nous avons écrit pour la géneration de la grille.

\subsection*{Mesure des performances}

\begin{figure}[h!]
\centering
\centerfloat
\begin{subfigure}{.7\textwidth}
  \centering
  \resizebox{8cm}{!}{%
	\begin{tikzpicture}[
		]
		\begin{axis}[
		pin/.style=fill=black,
			grid=major,
			xlabel=Largeur,
			ylabel=Hauteur,
			zlabel=Temps (secondes),
			max space between ticks=20
			]
		\addplot3 [only marks,scatter] file {results/30.avg.txt};

		\node[fill=white] at (axis cs: 1,6,200) {$\approx$ 2 sec};
		\node[fill=white] at (axis cs: 7.5,6,700) {$\approx$ 12 min};

		\end{axis}
	\end{tikzpicture}
	}
	\caption{Moyenne}
\end{subfigure}%
\begin{subfigure}{.7\textwidth}
  \centering
	\resizebox{8cm}{!}{%
	\begin{tikzpicture}[auto]
		\begin{axis}[
			grid=major,
			xlabel=Largeur,
			ylabel=Hauteur,
			zlabel=Temps (secondes),
			max space between ticks=20
			]
		\addplot3 [only marks,scatter] file {results/30.min.txt};

		\node[fill=white] at (axis cs: 7,6,80) {$\approx$ 1 min};

		\end{axis}
	\end{tikzpicture}
	}
	\caption{Minimum}
\end{subfigure}
	\caption{Temps de génération avec 30\% de cases noires}
	\label{fig:benchmark-time-30}
\end{figure}

Concernant la figure \ref{fig:benchmark-time-30} (par exemple), on constate que les données sont plutôt contre-intuitives. Certes, la génération
est extrêmement rapide sur les petites grilles (deux secondes en moyenne) mais
ne commence à prendre du temps que très brusquement, au voisinage des dimensions
$8 \times 8$, avec des valeurs maximales qui ne sont pas forcément là où on
les attend: le maximum est atteint pour $9 \times 6$ (douze minutes) alors
qu'une grille $9 \times 9$ n'a pris que 8 secondes!

En fait, il faut prendre ces résultats avec prudence: si toutes les machines
ont pu réaliser, dans le temps imparti, les grilles de petite taille, seules
quelques unes ont pu aller jusqu'à la grille $9 \times 9$. Il suffit de
regarder le graphe des temps minimum pour se rendre compte que, sur certaines
machines, mêmes les grilles les plus compliquées peuvent prendre moins de
6 secondes.

Il est extrêmement ardu d'obtenir des valeurs significatives devant le facteur
d'aléa des exépriences: le dictionnaire est très grand et l'algorithme dépend
fortement du placement des cases noires. Si on a \og de la chance \fg{}, notre
fonction d'évaluation placera rapidement des mots compatibles et n'aura pas besoin
de revenir souvent en arrière. Dans le cas contraire, la génération peut
s'éterniser (27\% des générations ont du être interrompues pendant les grilles $7 \times 7$).

% \vspace{1em}
\begin{figure}[ht!]
\centering
\centerfloat
\begin{subfigure}{.7\textwidth}
  \centering
  \resizebox{8cm}{!}{%
	\begin{tikzpicture}
		\begin{axis}[
			grid=major,
			xlabel=Largeur,
			ylabel=Hauteur,
			zlabel=Temps (secondes),
			max space between ticks=20
			]
		\addplot3 [only marks,scatter] file {results/20.avg.txt};

		\end{axis}
	\end{tikzpicture}
	}
	\caption{Moyenne}
\end{subfigure}%
\begin{subfigure}{.7\textwidth}
  \centering
	\resizebox{8cm}{!}{%
	\begin{tikzpicture}[auto]
		\begin{axis}[
			grid=major,
			xlabel=Largeur,
			ylabel=Hauteur,
			zlabel=Temps (secondes),
			max space between ticks=20
			]
		\addplot3 [only marks,scatter] file {results/20.min.txt};

		\end{axis}
	\end{tikzpicture}
	}
	\caption{Minimum}
\end{subfigure}
	\caption{Temps de génération avec 20\% de cases noires}
	\label{fig:benchmark-time-20}
\end{figure}

% \vspace{1em}

\begin{figure}[ht!]
\centering
\centerfloat
\begin{subfigure}{.7\textwidth}
  \centering
  \resizebox{8cm}{!}{%
	\begin{tikzpicture}
		\begin{axis}[
			grid=major,
			xlabel=Largeur,
			ylabel=Hauteur,
			zlabel=Temps (secondes),
			max space between ticks=20
			]
		\addplot3 [only marks,scatter] file {results/10.avg.txt};

		\end{axis}
	\end{tikzpicture}
	}
	\caption{Moyenne}
\end{subfigure}%
\begin{subfigure}{.7\textwidth}
  \centering
	\resizebox{8cm}{!}{%
	\begin{tikzpicture}[auto]
		\begin{axis}[
			grid=major,
			xlabel=Largeur,
			ylabel=Hauteur,
			zlabel=Temps (secondes),
			max space between ticks=20
			]
		\addplot3 [only marks,scatter] file {results/10.min.txt};

		\end{axis}
	\end{tikzpicture}
	}
	\caption{Minimum}
\end{subfigure}
	\caption{Temps de génération avec 10\% de cases noires}
	\label{fig:benchmark-time-10}
\end{figure}

Dans le cas 10\% de cases noires (figure \ref{fig:benchmark-time-10}), on constate que le minimum de temps
nécessaire est tout de même supérieur aux autres cas. C'est tout à fait normal,
les grilles contiennent plus de cases à remplir, donc ont moins de chance d'être
rapidement générées.

En conclusion de ces tests, on peut affirmer que notre algorithme présente
de bonnes performances pour les grilles de taille $9 \times 9$ et moins.
Dès qu'une des dimensions dépasse $10$, le temps de génération bondit à au moins
30 minutes en moyenne, ce qui devient beaucoup moins raisonnable. Toutefois,
si le temps n'est pas un problème, notre programme parvient à générer des
grilles d'une telle taille!
