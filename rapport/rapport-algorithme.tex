\label{probleme-algo}
Il y a deux possibilités en ce qui concerne le placement des cases noires: soit l'algorithme de
remplissage les place lui-même, à la volée lorsqu'il trouve cela nécessaire,
soit l'utilisateur choisit lui-même l'emplacement de celles-ci et l'algorithme essaye de remplir
les \og trous \fg{} au mieux.

Les deux méthodes ont leurs avantages et inconvénients.
En effet, si l'algorithme les place automatiquement, la décision de placer une case noire est
tout sauf triviale: si l'algorithme pose une case en \emph{dernier recours}, la génération
risque de s'éterniser faute de solution avec une case noire à cet endroit. Si au contraire
l'algorithme est pessimiste et pose facilement (rapidement) les cases noires, on peut se
retrouver avec des grilles présentant un fort pourcentage de cases noires, ce qui va à
l'encontre de l'énoncé et n'est pas intéressant pour l'utilisateur.
Ce dernier peut, par ailleurs, vouloir contrôler la structure de la grille, ce qui est
impossible en l'occurrence.

Laisser l'utilisateur choisir la position des cases noires permet de limiter leur nombre
et de choisir leur emplacement. Le prix de cette contrainte est sans doute une plus
grande chance de ne pas trouver de solution pour remplir la grille. Cela demande
aussi davantage de travail à l'utilisateur qui doit fournir le motif de cases noires
qu'il veut utiliser\footnote{Notre projet fournit un utilitaire en langage Python
qui permet de générer aléatoirement un tel motif pour une dimension et un taux de
cases noires donnés. Ce défaut est donc partiellement résolu.}.

Nous avons choisi cette deuxième solution pour notre projet. Nous sommes conscients
que cela présente un biais par rapport à l'impératif du sujet qui demande des grilles
avec un \emph{minimum de cases noires}, mais comme expliqué en introduction, ce
minimum n'étant pas calculable, il semble raisonnable de considérer le problème de
cette façon.

Nous avons décidé de remplir la grille case par case et ligne par ligne, en vérifiant les
contraintes décrites précédemment. Il faut donc définir une fonction
qui, à partir de la grille partiellement remplie, le dictionnaire (ou, en pratique,
l'index de recherche qui lui est associé) et une liste de mots déja utilisés dans
la grille remplisse la case dont le numéro est passé en paramètre de façon à ce
que la grille corresponde bien à une grille de mots-croisés.

La fonction renverra vrai si elle a trouvé une lettre qui permette de terminer de
remplir la grille, et faux sinon.

La liste de mots déjà utilisés n'est pas nécessaire car on peut obtenir cette
information à partir de la grille partiellement remplie. Mais cela engendrerait
beaucoup de calculs inutiles, ce que l'on a préferé éviter; nous avons donc
déployé une structure pour mémoriser cette liste.

\subsection{Implantation de l'algorithme}

L'analyse précédente permet d'envisager l'algorithme \ref{algo:remplir_rec},
qui est récursif. On remarque que la fonction utilise de nombreuses sous-fonctions
qui ne sont pas décrites explicitement dans ce rapport.

\begin{algorithm}
	\Entree{grille: Grille, indice: Indice, dico: IndexDeRecherche, utilisés: PileDeMots}
	\Sortie{Booléen}


\textbf{Fonction} \texttt{remplir_rec} \Deb{
	\Si{indice $ < $ taille(grille)}
	{
		prefl $\leftarrow$ préfixe_ligne(grille, indice)\\
		prefc $\leftarrow$ préfixe_colone(grille, indice)\\
		\Si{grille[indice] = CaseNoire}
		{
			\Si{NON est_un_mot_inutilisé(dico, utilisés, prefl) OU \\
			 ~~~NON est_un_mot_inutilisé(dico, utilisés, prefc) }
			{
				\Retour{Faux}
			}
			\Sinon
			{
				empiler(utilisés, prefl) \\
				empiler(utilisés, prefc) \\
				\Retour{remplir_rec(grille, indice + 1, dico, utilisés)}
			}
		}
		\Sinon
		{
			taille_mot_l $\leftarrow$ taille_requise_ligne(grille, indice)\\
			taille_mot_c $\leftarrow$ taille_requise_colone(grille, indice)\\
			liste $\leftarrow$ dico_liste_meilleurs_lettres(dico, prefc, prefl, \\
			~~~~~~~~~~~~~~~~~~~~~~~~taille_mot_l, taille_mot_c)\\
			succès $\leftarrow$ Faux \\
			\Pour{$ lettre \in liste $}
			{
				supprimer_mots_inutilisés(utilisés, indice) \\
				grille[indice] $\leftarrow$ lettre \\
				succès $\leftarrow$ test_bords(grille, indice, dico, utilisés)\\
				\Si{succès}{succès $\leftarrow$ remplir_rec(grille, indice + 1, dico, utilisés)}
			}
			\Retour{succès}
		}
	}
	\Sinon {
		\Retour{Vrai}
	}
	}
	\caption{Algorithme de remplissage}
	\label{algo:remplir_rec}
\end{algorithm}

L'algorithme \ref{algo:remplir_rec} appelle la fonction \code{test_bord}, qui vérifie si la condition de fin de ligne et de colonne d'une grille de mots-croisés est vérifiée. Cela fait normalement partie intégrante de l'algorithme, mais nous avons dû la séparer pour des raisions de modularité et lisibilité du code. Cette fonction est expliquée à l'algorithme \ref{algo:test_bords}.

\begin{algorithm}
	\caption{Fonction de test des bords}
	\Entree{grille: Grille, indice: Indice, dico: IndexDeRecherche, utilisés: PileDeMots}
	\Sortie{Booléen}

\label{algo:test_bords}

	\textbf{Fonction} \code{test_bords}\\

	\Si{indice est en fin de ligne}
	{
		mot $\leftarrow$ préfixe_ligne(grille, indice) \\
		\Si{est_dans_alphabet(grille[indice])}
		{
			mot $\leftarrow$ mot.grille[indice]
		}
		\Si{NON est_un_mot_inutilisé(dico, utilisés, mot)}
			 {\Retour{Faux}}
		\Sinon
			 {empiler(utilisés, mot)}
	}
	\Si{indice est en fin de colone}
	{
		mot $\leftarrow$ préfixe_colone(grille, indice) \\
		\Si{est_dans_alphabet(grille[indice])}
		{
			mot $\leftarrow$ mot.grille[indice]
		}
		\Si{NON est_un_mot_inutilisé(dico, utilisés, mot)}
			 {\Retour{Faux}}
		\Sinon
			 {empiler(utilisés, mot)}
	}
	\Retour{Vrai}
\end{algorithm}

\subsection{Complexité en temps}

La détermination de la complexité en temps est assez difficile, car elle dépend
de la taille de la grille, de la taille du dictionnaire, du nombre de cases noires
et de la dégénerescence de l'arbre de recherche dans le dictionnaire.

Nous comptons le nombre d'écriture dans la grille dans le pire des cas (cas
théorique qui ne se produit pas en pratique). La grille est de taille $ n = longueur
\times largeur $, et soit $ m $ le nombre de cases noires. Le dictionnaire est
supposé ne contenir que des mots de taille trop grande pour la grille, et proposer
toutes les lettres de l'alphabet comme suite à un préfixe donné. Nous obtenons
donc la formule de récurrence suivante, pour un indice $ i $ de la grille:

$C(i) = \left\{ \begin{array}{cl}
1 + 26 \times C(i + 1) & \text{si grille[i] n'est pas une case noire et } i < n \\
0                      & \text{sinon}
\end{array} \right.$

Une résolution simple donne une complexité en \complexite{26^{n - m}}, ce qui est mauvais,
mais explicable par le fait que quand aucune solution n'est trouvable (ce qui fait
partie des hypothèses de départ), l'algorithme essaie de placer tout les mots du
dictionnaire dans tous les endroits possibles avant de retourner une indication d'échec.

\subsection{Espace utilisé}

Comme dit précédement, nous utilisons trois structures pour remplir la grille:

\begin{itemize}
\item{La grille elle-même est de taille $ longueur \times largeur $.}
\item{La pile de mots a un taille variable, mais majorée par le nombre de cases dans
la grille.}
\item{L'index de recherche a un taille qui dépend du dictionnaire. En effet, le nombre de
feuilles de l'arbre est proportionnel au nombre de mots, et sa profondeur est égale à la
taille du plus grand mot.}
\end{itemize}

Avec un dictionnaire comme celui avec lequel nous avons fait nos tests, de 400~000 mots
environs (chaque mots faisant moins de 22 lettres), l'index de recherche fait plusieurs
dizaines de Méga octets. La grille et la pile de mots font une taille négligeable face
à cela.

\subsection{Terminaison}
En algorithmique se pose la question de la terminaison d'un programme: pour
un ensemble de données en entrée, l'algorithme va-t-il \emph{finir}, c'est-à-dire
ne pas entrer dans une boucle infinine (récursive ou itérative)?

Rappelons que notre algorithme est constitué d'une fonction récursive qui s'appelle
en son sein lors d'une boucle sur un ensemble fini de lettres. La terminaison itérative
est donc trivialement vérifiée.

La terminaison de la fonction récursive \code{remplir_rec} (algorithme \ref{algo:remplir_rec}) se fait assez facilement.
\begin{itemize}
\item Pour une grille de taille $1 \times 1$ (une seule case), la preuve est immédiate:
si cette unique case est une case noire, on retourne \emph{Vrai} dès le premier appel
récursif (ligne 12). Sinon, on la remplit avec la première lettre possible et on retourne
(lignes 24 et 25).
\item Suppons prouvé la terminaison pour une grille de taille $N \times M$. Nous commençons
à remplir le premier indice: $i = 1$. Que la case à cet indice soit noire ou vide ne change rien:
soit on retourne un booléen, soit on appelle \code{remplir_rec} avec l'indice $i + 1$.
Le nombre de cases à remplir diminue donc de un. On rappelle que l'algorithme termine quand
$i > N \ times M$; il devient donc évident que, par induction, l'algorithme sur la grille
$N \times M$ termine.
\end{itemize}
Nous avons ainsi prouvé par récurrence que l'algorithme termine pour toute taille de grille.

Il est toutefois important de noter que, si l'algorithme termine toujours en théorie,
le temps d'éxecution peut être extrêmement long (cf. partie \ref{tests}), atteignant
par exemple 30 minutes pour des grilles $10 \times 10$ sur un ordinateur réçent et
augmentant de façon exponentielle en la dimension des grilles.


\subsection{Correction}
La deuxième question qui se pose est celle de la correction du programme:
notre algorithme produit-il des grilles de mots-croisés \emph{correctes},
c'est-à-dire vérifiant les contraintes énoncées en introduction?

La preuve est simple si l'on remarque que par construction, l'algorithme ne
retourne \emph{Vrai} que quand il est arrivé au dernier indice et que tous
les mots de la grille appartiennent au dictionnaire (c'est le rôle de la fonction
ligne 16 de retourner une liste de lettres valides pour le préfixe donné).

Pour appuyer ce propos, nous avons profité des tests (partie \ref{tests}) pour
analyser les milliers de grilles générées, grâce à un script Python\footnote{Source
disponible dans \code{meta/checkgrids.py}}. Ce script lit les grilles générées
au cours des tests et vérifie:
\begin{itemize}
\item que chaque mot de plus de deux lettres n'apparaît pas en double;
\item que chaque mot appartient au dictionnaire utilisé pour la génération.
\end{itemize}
\vspace{1em}
Le résultat prend la forme suivante:
\vspace{-1em}
\begin{wideverbatim}
grids/5x7.brownie.html             WORDS 19  NO-DUPLICATES YES  ALL-EXIST YES  OK YES
grids/5x7.bulls.html               WORDS 19  NO-DUPLICATES YES  ALL-EXIST YES  OK YES
grids/5x7.capucin.html             WORDS 18  NO-DUPLICATES YES  ALL-EXIST YES  OK YES
grids/5x7.cardinals.html           WORDS 17  NO-DUPLICATES YES  ALL-EXIST YES  OK YES
grids/5x7.carnot.html              WORDS 21  NO-DUPLICATES YES  ALL-EXIST YES  OK YES
grids/5x7.casimirperrier.html      WORDS 20  NO-DUPLICATES YES  ALL-EXIST YES  OK YES
\end{wideverbatim}
La grille est parfaitement valide si \code{OK YES} apparaît sur la ligne.

17~304 (!) grilles ont été vérifées. 17~288 d'entre elles sont \og OK YES \fg{}
donc valides. Il reste alors 21 grilles, soit 0,12\%, pour lesquelles les mots sont
bien dans le dictionnaire mais présentent un double sur un mot de trois lettres
à chaque fois. Malgré des recherches approfondies, nous n'avons pas trouvé
la cause de ces erreurs, qui restent très isolées. Ces erreurs sont par
ailleurs moins \emph{graves} que si les mots n'existaient pas.

Nous concluerons ainsi que les grilles générées contiennent exclusivement des
mots du dictionnaire et qu'il existe un risque négligeable ($\approx 0,10\%$)
d'obtenir deux fois le même mot.
