PROJECT_NAME = crosswords

CC = gcc
CFLAGS = -Wall -std=c99

OBJ =	main.o \
	builder_fixed.o \
	memory_dynamic.o \
	eval.o \
	dico_search.o \
	grid.o \
	grid_io.o \
	word_list.o \
	dico_node.o \
	utils.o \
	stack.o

all : $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(PROJECT_NAME)

%.o : src/%.c
	$(CC) $(CFLAGS) -c $< -o $@

clean :
	rm *.o

# Pour le rapport

REPORT_DIR = rapport
REPORT_SRC = rapport.tex
REPORT_OBJ = rapport-$(PROJECT_NAME).pdf

report : $(REPORT_DIR)/$(REPORT_SRC)
	cd $(REPORT_DIR) && latexmk -pdf $(REPORT_SRC)
	mv $(REPORT_DIR)/$(REPORT_SRC:.tex=.pdf) $(REPORT_OBJ)

clean-report :
	cd $(REPORT_DIR) && latexmk -c
