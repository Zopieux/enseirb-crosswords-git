#include "utils.h"

int
min(int a, int b) {
	return (a < b) ? a : b;
}

int
max(int a, int b) {
	return (a < b) ? b : a;
}

int
get_index_max(int array[], int size) {
	int ret = 0;
	for(int i = 1; i < size; i++) {
		if(array[ret] < array[i])
			ret = i;
	}
	return ret;
}

void
op_scal(int ar1[], int ar2[], int dest[], int size, OP_FUNC op) {
	for(int i = 0; i < size; i++) {
		dest[i] = op(ar1[i], ar2[i]);
	}
}

int
sort_index_desc_pos(int src[], int src_size, char dest[], int offset) {
	int id = 0;
	int ret = 0;
	while(0 < src[id = get_index_max(src, src_size)]) {
		src[id] = 0;
		dest[ret] = id + offset;
		ret++;
	}
	return ret;
}
