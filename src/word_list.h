/*
Création d'une liste de mots à partir d'un fichier texte contenant un
dictionnaire.
*/

#ifndef DICO_FILE_H_
#define DICO_FILE_H_

#include <stdio.h>

// NOUVELLES FONCTIONS

typedef struct WordList WordList;
struct WordList {
	int size;
	FILE* file;
	int* offset;
};

WordList* word_list_new_from_file(char* fname);

void word_list_delete(WordList* wl);

/*
Fonction : wordlist_get
Description : retourne le mot d'indice index
Retour : l'adresse de buffer si réussite, NULL sinon
*/
char* word_list_get(WordList* wl, int index, char* buffer, int buf_size);

int word_list_get_size(WordList* wl);

#endif
