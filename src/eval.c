#include "eval.h"

#include <stdlib.h>

#include "const.h"
#include "dico_node.h"

int occur[MAX_WORD_SIZE][ALPHABET_LENGTH];

void
get_eval(DICO* dico, int c, int cpos) {
	// on compte les occurences des lettres par positions dans le mot
	if(dico != NULL) {
		if(cpos >= 0)
			occur[cpos][c] += dico_node_get_word_count(dico);
		for(int i = 0; i < ALPHABET_LENGTH; i++)
		 	get_eval(dico_node_get_child_by_index(dico, i), i, cpos + 1);
	}
}


void
set_eval(DICO* dico, int c, int cpos) {
	// on écrit dans l'arbre
	if(dico != NULL) {
		if(cpos >= 0)
		  dico_node_set_eval(dico, occur[cpos][c]);
		for(int i = 0; i < ALPHABET_LENGTH; i++)
			set_eval(dico_node_get_child_by_index(dico, i), i, cpos + 1);
	}
}

void
dico_search_init_eval(DICO* dico) {
	get_eval(dico, 0, -1);
	set_eval(dico, 0, -1);
}
