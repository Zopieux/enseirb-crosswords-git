/*
Structure de l'API d'allocation mémoire.
*/

#ifndef MEMORY_H_
#define MEMORY_H_

#include <stdlib.h> // définition de size_t

/*
Fonction : memory_init
Description :
*/
void memory_init();

/*
Fonction : memory_alloc
Description : alloue une zone de mémoire de la taille indiquée
Arguments :
	size : taille en octets de l'espace à allouer
Retour :
	l'addresse de la variable
*/
void* memory_alloc(size_t size);

/*
Fonction : memory_free
Description : libère la mémoire allouée
Arguments :
	ptr : pointeur vers la variable à supprimer
*/
void memory_free(void* ptr);

/*
Function : memory_quit
Description : libère toute la mémoire non encore libérée
*/
void memory_quit();

#endif
