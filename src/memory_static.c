#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "const.h"
#include "memory.h"

/*
Libraire d'émulation d'allocation dynamique de la mémoire
(qui n'est pas autorisée dans le projet).

==> NE PAS COMPILER AVEC "memory_dynamic.c" <==

*/

typedef struct MemElem MemElem;
struct MemElem {
	MemElem* next;
	size_t data_size; // taille des données en octets
};

char heap[MAX_MEM_SIZE];

MemElem* lbegin;
MemElem* lend;

// UTILS

/*
Retourne l'espace mémoire libre après l'élément `elem`.
*/
size_t mem_get_size_after(MemElem* elem) {
	void* end_block = (void*)elem->next;
	if(end_block == NULL) end_block = (void*)(heap + MAX_MEM_SIZE);
	return end_block - ((void*)elem + sizeof(MemElem) + elem->data_size);
}

/*
Retourne un pointeur vers le premier MemElem qui contient au moins `size`
de place entre lui et son suivant.
*/
MemElem* mem_get_fit(size_t size) {
	MemElem* elem = lbegin;
	_Bool fit = false;
	while(elem != NULL && !(fit = mem_get_size_after(elem) >= size)) {
		elem = elem->next;
	}
	if(fit) return elem;
	return NULL;
}

/*
Retourne un pointeur vers le dernier MemElem
*/
MemElem* mem_get_last(size_t size) {
	if(mem_get_size_after(lend) >= size) return lend;
	else return NULL;
}

////

void memory_init() {
	lbegin = (MemElem*)heap;
	lbegin->next = NULL;
	lbegin->data_size = 0;
	lend = lbegin;
}

/*
Alloue `size` octets de mémoire. Emulation de `malloc`.
*/
void* memory_alloc(size_t size) {
	//// ATTENTION : DEUX METHODES DE RECHERCHE D'ADRESSE !!
	// MemElem* prev = mem_get_fit(size + sizeof(MemElem));
	MemElem* prev = mem_get_last(size + sizeof(MemElem));
	////
	if(prev == NULL) return NULL;
	
	MemElem* var = (MemElem*)((char*)prev + sizeof(MemElem) + prev->data_size);
	
	var->next = prev->next;
	prev->next = var;
	
	var->data_size = size;
	
	lend = var;
	
	return (void*)((char*)var + sizeof(MemElem));
}

/*
Libère l'espace alloué pour `ptr`. Emulation de `free`.
*/
void memory_free(void* ptr) {
	MemElem* var = (MemElem*)(((char*)ptr) - sizeof(MemElem));
	
	MemElem* prev = lbegin;
	while(prev != NULL && prev->next != var) {
		prev = prev->next;
	}
	
	if(lend == var) lend = prev;
	
	
	prev->next = var->next;
}

void memory_quit() {
	// Pas utile, car la mémoire est effacée à la fin du programme ou à l'appel de memory_init
	lbegin->next = NULL;
}

// DEBUGGAGE

size_t memory_get_empty_space()
{
	/*MemElem* elem = lbegin;
	size_t ret = 0;
	while(elem != NULL) {
		ret += mem_get_size_after(elem);
		elem = elem->next;
	}
	return ret;*/
	return MAX_MEM_SIZE - ((char*)lend - (char*)lbegin);
}

void memory_print() {
	MemElem* elem = lbegin;
	printf("-- Memory Begin --\n");
	while(elem != NULL) {
		printf("[%p] : %lu-%lu (%lu) \n", elem, (size_t)((char*)elem-heap), (size_t)((char*)elem-heap)+elem->data_size+sizeof(MemElem), elem->data_size+sizeof(MemElem));
		if(mem_get_size_after(elem) > 0)
			printf("[........] : %lu-%lu (%lu)\n", (size_t)((char*)elem-heap)+elem->data_size+sizeof(MemElem), (size_t)((char*)elem-heap+mem_get_size_after(elem)+sizeof(MemElem)+elem->data_size), mem_get_size_after(elem));
		elem = elem->next;
	}
	printf("--  Memory End  --\n");
}
