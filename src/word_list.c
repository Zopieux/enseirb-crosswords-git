#include "word_list.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "const.h"
#include "dico_search.h"
#include "memory.h"
#include "utils.h"

// NOUVELLES FONCTIONS PUBLIQUES

WordList*
word_list_new_from_file(char* fname) {
	FILE* wlf = NULL;
	char buffer[BUFFER_SIZE];
	WordList* wl = NULL;
	int i = 0;

	wlf = fopen(fname, "r");

	if(!wlf) {
		fprintf(stderr, "ERROR: unable to open file (%s)",
			strerror(errno));
		return NULL;
	}

	wl = memory_alloc(sizeof(WordList));

	fgets(buffer, BUFFER_SIZE, wlf);
	wl->size = atoi(buffer);

	wl->offset = memory_alloc(sizeof(int) * (wl->size));

	while(!feof(wlf) && i < wl->size) {
		wl->offset[i] = ftell(wlf);
		fgets(buffer, BUFFER_SIZE, wlf);
		i++;
	}

	if(i != wl->size) {
		perror("ERROR: not enought words in file");
		word_list_delete(wl);
	}

	wl->file = wlf;

	// on ne ferme pas le fichier car il est stocké dans WordList
	return wl;
}

void
word_list_delete(WordList* wl) {
	fclose(wl->file);
	memory_free(wl->offset);
	memory_free(wl);
}

char*
word_list_get(WordList* wl, int index, char* buffer, int buf_size) {
	int i = 0;

	if(wl && 0 <= index && index < wl->size && buffer) {
		if(fseek(wl->file, wl->offset[index], SEEK_SET) != 0)
			return NULL;

		fgets(buffer, buf_size, wl->file);

		while(buffer[i] != '\n' && buffer[i] != '\0' && i < buf_size-1)
			i++;

		buffer[i] = '\0';

		return buffer;
	}
	else
		return NULL;
}

int
word_list_get_size(WordList* wl) {
	if(wl)
		return wl->size;
	else
		return ERROR_NULL_ARG;
}
