#ifndef MEMORY_STATIC_DEBUG_H
#define MEMORY_STATIC_DEBUG_H

#include "memory.h"

//!\\ Les fonction suivantes ne sont pas utilisables en mode dynamique
// Fonctions de débuggage

/*
Fonction : memory_get_empty_space
Description : Donne l'espace encore disponible pour l'allocation
*/
size_t memory_dbg_get_empty_space();

/*
Fonction : memory_print
Descripton : affiche l'état de la mémoire entre les octets de numéro begin et end
*/
void memory_dbg_print();

#endif
