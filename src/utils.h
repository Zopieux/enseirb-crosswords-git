/*
Petites fonctions-outils.
*/

#ifndef UTILS_H_
#define UTILS_H_

// Retourne le minimum des entiers a et b
int min(int a, int b);

// Retourne le maximum des entiers a et b
int max(int a, int b);

// Retourne l'index du max du tableau
int get_index_max(int array[], int size);

// Type fonction d'opération, pour op_scal notamment
typedef int (*OP_FUNC)(int, int);

// Opération scalaire sur tableaux
// exemple : produit scalaire ==> op_scal(vec1, vec2, vec3, n, produit)
void op_scal(int ar1[], int ar2[], int dest[], int size, OP_FUNC op);

// Trie les indices par ordre décroissant de valeur
// (supprime les indices de valeur nulle ou négative)
// retourne la taille de dest[]
int sort_index_desc_pos(int src[], int src_size, char dest[], int offset);

#endif