#ifndef BUILDER_BLACK_FIXED_F
#define BUILDER_BLACK_FIXED_F

#include <stdbool.h>

#include "dico_node.h"
#include "grid.h"

/*
Fonction principale de construction de la grille, étant donné une grille (avec
les cases noires déjà en place) et un dictionnaire.
*/
bool build_grid(Grid*, DICO*);

#endif
