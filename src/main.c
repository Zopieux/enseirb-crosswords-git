#include <argp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "builder_fixed.h"
#include "eval.h"
#include "grid.h"
#include "grid_io.h"
#include "word_list.h"
#include "dico_node.h"
#include "dico_search.h"
#include "memory.h"

// Documentatioon rapide
static char doc[] =
	"Crosswords -- generates a grid from a dictionnary";

// Arguments acceptés
static char args_doc[] = "<grid path> <dictionnary path>";

// Options acceptées
static struct argp_option options[] = {
	{"output", 'o', "FILE", 0,
		"Writes output in FILE rather than the default standard output."},
	{"format", 'f', "FORMAT", 0,
		"Writes output using FORMAT (possible values: " AVAILABLE_GRID_FORMATS_STR ")."},
	{0}
};

#define NUM_ARGS 2

struct arguments {
	char* args[NUM_ARGS];
	FILE* output_file;
	char* output_format;
};

// Ouvre un fichier en écriture ou affiche un joli message d'erreur
static FILE*
file_open_write(struct argp_state *state, char* filename, char* msg) {
	FILE* fileptr = fopen(filename, "wb");

	if(fileptr == NULL)
		argp_error(state,
			"I/O error: unable to open %s file. Check permissions?", msg);

	return fileptr;
}

// Parse une option (callback pour argp_parse)
static error_t
parse_opt(int key, char *arg, struct argp_state *state) {
	/* Récupérer l'argument depuis argp_parse qui est un pointeur vers la
	structure arguments */
	struct arguments *arguments = state->input;
	bool accepted = false;

	switch (key) {
		case 'o': // option output_file : on change le FILE*
			arguments->output_file = file_open_write(state, arg, "output");
			break;

		case 'f': // option format de sortie : on vérifie qu'il existe
			for(int i = 0; i < AVAILABLE_GRID_FORMATS_LEN; i++) {
				if(strcmp(arg, AVAILABLE_GRID_FORMATS[i]) == 0) {
					accepted = true;
					arguments->output_format = arg;
					break;
				}
			}
			if(!accepted)
				argp_error(state, "FORMAT must be one of " AVAILABLE_GRID_FORMATS_STR ".\n");
			break;

		case ARGP_KEY_ARG: // un des chemins de fichier (grille, dico)
			if (state->arg_num >= NUM_ARGS) {
				// Trop d'arguments !
				argp_usage(state);
			}
			arguments->args[state->arg_num] = arg;
			break;

		case ARGP_KEY_END:
			if (state->arg_num < NUM_ARGS) {
				// Et là pas assez d'arguments !
				argp_usage(state);
			}
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

// Pour le parser
static struct argp argp = {options, parse_opt, args_doc, doc};

// I CAN HAZ CHEEZBURGMAIN?
int
main(int argc, char** argv) {

	struct arguments arguments;

	// Options par défaut
	arguments.output_file = stdout;
	arguments.output_format = AVAILABLE_GRID_FORMATS_DEFAULT;

	/* On parse la ligne de commande. Après cette ligne, il est assuré que
	`arguments` contient des arguments valides. */
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	memory_init();

	Grid* grid;
	// Création de la grille
	// - représente l'entrée standard
	if(strcmp(arguments.args[0], "-") == 0)
		grid = grid_new_from_buffer(stdin);
	else
		grid = grid_new_from_filename(arguments.args[0]);

	if(grid == NULL) {
		if(arguments.output_file != stdout)
			fclose(arguments.output_file);
		memory_quit();
		return EXIT_FAILURE;
	}

	// Construction du dictionnaire
	WordList* wl = word_list_new_from_file(arguments.args[1]);
	if(wl == NULL) {
		if(arguments.output_file != stdout)
			fclose(arguments.output_file);
		grid_delete(grid);
		memory_quit();
		return EXIT_FAILURE;
	}

	DICO* dico = dico_search_new_from_word_list(wl);
	dico_search_init_eval(dico);

	// Appel principal : construction de la grille !
	build_grid(grid, dico);

	if(strcmp(arguments.output_format, "ascii") == 0)
		grid_write_ascii(grid, arguments.output_file);
	else if(strcmp(arguments.output_format, "html") == 0)
		grid_write_html(grid, arguments.output_file);
	else if(strcmp(arguments.output_format, "latex") == 0)
		grid_write_latex(grid, arguments.output_file);

	if(arguments.output_file != stdout)
		fclose(arguments.output_file);

	// Nettoyage
	dico_search_delete(dico);
	word_list_delete(wl);
	grid_delete(grid);
	memory_quit();

	return EXIT_SUCCESS;
}
