#include "builder_fixed.h"

#include <string.h>
#include <stdbool.h>

#include "const.h"
#include "dico_node.h"
#include "dico_search.h"
#include "word_list.h"
#include "eval.h"
#include "grid.h"
#include "grid_io.h"
#include "stack.h"


/*
Structure pour se rapeller du mot et de l'index après lequel
on ne doit plus l'utiliser
*/
struct UsedWord {
	char word[MAX_WORD_SIZE];
	int index;
};

struct UsedWord*
used_word_fill(struct UsedWord* uw, char word[MAX_WORD_SIZE], int index) {
	memcpy(uw->word, word, MAX_WORD_SIZE);
	uw->index = index;
	return uw;
}


// teste si ce sont les mêmes mots (sans tenir compte de l'indice)
int
same_word(void* w1, void* w2) {
	struct UsedWord *uw1 = w1, *uw2 = w2;
	return !strcmp(uw1->word, uw2->word);
}


// supprime les mots qui ne sont plus utilisés
void
used_words_stack_update(struct Stack* used_words, int index) {
	while(!stack_is_empty(used_words)                                &&
	index <= ((struct UsedWord*)stack_head(used_words))->index )
		stack_pop(used_words);
}


// Teste si c'est bien un mot du dico, et si il n'est pas utilisé
#define MACRO_IS_WORD_UNUSED(dico, used_words, uw, word, index) \
			(dico_search_is_word(dico, word) &&	\
			!stack_is_element(used_words, \
			used_word_fill(&uw, word, index), same_word))

// Teste si le mot est de taille au moins deux avant de le mettre dans la pile
#define MACRO_STACK_PUSH(used_words, uw, word, index) if(1 < strlen(word)) \
		stack_push(used_words, used_word_fill(&uw, word, index))


bool
test_bounds(Grid* grid, int ind, DICO* dico, struct Stack* used_words) {
	char word[MAX_WORD_SIZE];
	struct UsedWord uw;

	// Si on est en fin de ligne
	if(ind % grid_get_width(grid) == grid_get_width(grid) - 1) {
		// Vérifier l'existence du mot de la ligne
		// et si le mot n'est pas utilisé
		grid_get_prefix_line(grid, ind, word);

		if(IS_IN_ALPHABET(grid_get_by_index(grid, ind))) {
			int len = strlen(word);
			word[len] = grid_get_by_index(grid, ind);
			word[len + 1] = '\0';
		}

		if(!MACRO_IS_WORD_UNUSED(dico, used_words, uw, word, ind))
			return false;
		else
			MACRO_STACK_PUSH(used_words, uw, word, ind);
	}
	// Si on est en fin de colone
	if(grid_get_width(grid) * (grid_get_height(grid) - 1) <= ind) {
		// Vérifier l'existence du mot de la colone
		// et si le mot n'est pas utilisé
		grid_get_prefix_column(grid, ind, word);

		if(IS_IN_ALPHABET(grid_get_by_index(grid, ind))) {
			int len = strlen(word);
			word[len] = grid_get_by_index(grid, ind);
			word[len + 1] = '\0';
		}

		if(!MACRO_IS_WORD_UNUSED(dico, used_words, uw, word, ind))
			return false;
		else
			MACRO_STACK_PUSH(used_words, uw, word, ind);
	}

	return true;
}

bool
build_aux(Grid* grid, int ind, DICO* dico, struct Stack* used_words) {
	struct UsedWord uw;

	// Si on a pas fini la grille
	if(ind < grid_get_size(grid)) {
		char pl[MAX_WORD_SIZE], pc[MAX_WORD_SIZE];

		// Si on est sur une case noire
		if(grid_get_by_index(grid, ind) == BLACK_SQUARE) {
			// Vérification de l'existence des mots dans le dico
			// et si le mot n'est pas utilisé
			grid_get_prefix_line(grid, ind, pl);
			grid_get_prefix_column(grid, ind, pc);

			if(!MACRO_IS_WORD_UNUSED(dico, used_words, uw, pl, ind) ||
	 			!MACRO_IS_WORD_UNUSED(dico, used_words, uw, pc, ind))
				return false;
			else {
				MACRO_STACK_PUSH(used_words, uw, pl, ind);
				MACRO_STACK_PUSH(used_words, uw, pc, ind);
				return build_aux(grid, ind+1, dico, used_words);
			}
		}
		// Si on est sur une case normale
		else {
			// Récupération de la taille requise et des préfixes
			int sl = grid_get_word_size_line(grid, ind);
			int sc = grid_get_word_size_column(grid, ind);

			grid_get_prefix_line(grid, ind, pl);
			grid_get_prefix_column(grid, ind, pc);

			// recherche des lettres disponibles (et classées)
			char rc[ALPHABET_LENGTH];
			int rc_size = dico_search_prefix_size_sort_best(dico, pc, pl,
				sc, sl, rc);
			bool success = false;

			// on teste avec chaque lettre :
			for(int i = 0 ; i < rc_size && !success ; i++) {
				// Mise à jour des mots utilisés
				used_words_stack_update(used_words, ind);
				// Ecriture de la lettre et passage au suivant
				grid_set_by_index(grid, ind, rc[i]);
				success = test_bounds(grid, ind, dico, used_words);
				if(success)
					success = build_aux(grid, ind+1, dico, used_words);
			}
			return success;
		}
	}
	// Grille finie, on a réussi !
	else
		return true;
}

bool
build_grid(Grid* grid, DICO* dico) {
	bool ret = false;

	struct Stack* used_words = stack_new(sizeof(struct UsedWord));
	ret = build_aux(grid, 0, dico, used_words);
	stack_delete(used_words);

	return ret;
}
