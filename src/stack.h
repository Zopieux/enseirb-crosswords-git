#ifndef STACK_H
#define STACK_H

#include <stdlib.h>

/*
Structure de pile générique

Les éléments insérés sont copiés, donc toute modification ultérieur à push sont sans effet sur l'élément de la pile !
En revanche, la pile n'est pas copiée par push et pop.
*/

struct Stack;

struct Stack* stack_new(size_t elem_size);
void stack_delete(struct Stack* stack);

void* stack_head(struct Stack* stack);

// retourne le pointeur stack
struct Stack* stack_push(struct Stack* stack, void* elem);
struct Stack* stack_pop(struct Stack* stack);

int stack_is_empty(struct Stack* stack);

// Recherche dans la pile

// fonction qui renvoie TRUE si les deux éléments sont identiques, FALSE sinon
typedef int (*CMP_FUNC)(void*, void*);

int stack_is_element(struct Stack* stack, void* elem, CMP_FUNC cmp);

#endif
