// Constantes de représentation
#define BLACK_SQUARE '#'
#define EMPTY_SQUARE ' '

// Constantes liées à l'algo
#define MAX_WORD_SIZE 22
#define MAX_GRID_WIDTH 12
#define MAX_GRID_HEIGHT 12
#define MAX_DICO_SIZE 400000

// Constantes pour l'émulation mémoire
#define MAX_MEM_SIZE 100000000

// Constantes liées à l'alphabet (au cas où...)
#define ALPHABET_LENGTH ('Z' - 'A') + 1
#define ALPHA_CHAR 'A'

#define IS_IN_ALPHABET(var) ('A' <= var && var <= 'Z')

#define MACRO_GET_INDEX(c) (c - ALPHA_CHAR)

// Taille du buffer pour lecture dans un fichier :
#define BUFFER_SIZE 1024

// Erreurs
#define ERROR_NULL_ARG  -1
#define ERROR_NULL_FILE -2
#define ERROR_NULL_GRID -3
#define ERROR_FSEEK     -4
#define ERROR_GRID_SIZE -5

// Configuration de Stack
#define STACK_INIT_SIZE 32

// Valeurs booléennes
#define TRUE 1
#define FALSE 0
