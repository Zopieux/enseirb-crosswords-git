#ifndef EVAL_H_
#define EVAL_H_

#include "dico_search.h"

/*
Affecte récursivement à chaque noeud la fonction d'évaluation qui
correspond à sa lettre.
*/
void dico_search_init_eval(DICO* dico);

#endif