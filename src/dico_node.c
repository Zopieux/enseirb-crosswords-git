#include "dico_node.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "const.h"
#include "memory.h"

#define ERROR_MSG(ptr, fname) if(nod) printf("ERROR : [%s] NULL pointer as argument\n", fname)

struct DicoNode {
	int is_word;    // si le noeur représente bien un mot
	int num_leaf;   // nombre de mots qui contiennent ce noeud
	int flag_size;  // savoir si un mot de la bonne taille existe
	int eval;       // retour de la fonction d'évaluation
	struct DicoNode* child[ALPHABET_LENGTH];
};

// constructeur/destructeur

struct DicoNode*
dico_node_new() {
	struct DicoNode* ret = memory_alloc(sizeof(struct DicoNode));
	ret->is_word = 0;
	ret->num_leaf = 0;
	ret->flag_size = 0;
	ret->eval = 0;
	for(int i = 0; i < ALPHABET_LENGTH; i++)
		ret->child[i] = NULL;
	return ret;
}

void
dico_node_delete(struct DicoNode* nod) {
	if(nod) {
		for(int i = 0; i < ALPHABET_LENGTH; i++)
			dico_node_delete(dico_node_get_child_by_index(nod, i));
		memory_free(nod);
	}
}

// accesseurs

int
dico_node_get_word_count(struct DicoNode* nod) {
	return nod->num_leaf;
}

void
dico_node_add_word_count(struct DicoNode* nod) {
	nod->num_leaf++;
}

int
dico_node_accept_word_size(struct DicoNode* nod, int size) {
	return nod->flag_size & (1 << size);
}

void
dico_node_add_word_size(struct DicoNode* nod, int size) {
	nod->flag_size |= 1 << size;
}

int
dico_node_get_eval(struct DicoNode* nod) {
	return nod->eval;
}

void
dico_node_set_eval(struct DicoNode* nod, int eval) {
	nod->eval = eval;
}

int
dico_node_is_word(struct DicoNode* nod) {
	return nod->is_word;
}

void
dico_node_set_word(struct DicoNode* nod) {
	nod->is_word = 1;
}

void
dico_node_unset_word(struct DicoNode* nod) {
	nod->is_word = 0;
}

// recherche
struct DicoNode*
dico_node_get_child_by_index(struct DicoNode* nod, int index) {
	if(nod && 0 <= index && index < ALPHABET_LENGTH)
		return nod->child[index];
	else
		return NULL;
}

struct DicoNode*
dico_node_get_child(struct DicoNode* nod, char letter) {
	if(nod && IS_IN_ALPHABET(letter))
		return nod->child[MACRO_GET_INDEX(letter)];
	else
		return NULL;
}

struct DicoNode*
dico_node_get_child_by_prefix(struct DicoNode* nod, char *prefix) {
	struct DicoNode* ret = nod;

	if(!nod || !prefix)
		return NULL;

	while(*prefix != '\0' && ret != NULL) {
		ret = dico_node_get_child(ret, *prefix);
		prefix++;
	}

	return ret;
}

// construction

void
dico_node_add_child(struct DicoNode* nod, char letter) {
	if(IS_IN_ALPHABET(letter)) {
		if(dico_node_get_child(nod, letter)) {
			dico_node_delete(dico_node_get_child(nod, letter));
		}
		nod->child[MACRO_GET_INDEX(letter)] = dico_node_new();
	}
}

void
dico_node_add_child_word(struct DicoNode* nod, char *word) {
	int size = strlen(word);
	for(int i = 0; i < size; i++) {
		dico_node_add_word_count(nod);
		dico_node_add_word_size(nod, size);

		if(IS_IN_ALPHABET(word[i]) && !dico_node_get_child(nod, word[i]))
			dico_node_add_child(nod, word[i]);

		nod = dico_node_get_child(nod, word[i]);
	}
	dico_node_set_word(nod);
}

////
void
dico_node_get_eval_array(struct DicoNode* nod, int size, EVAL_ARRAY eval) {
	struct DicoNode* child = NULL;

	for(int i = 0; i < ALPHABET_LENGTH; i++) {
		if(
			nod &&
			(child = dico_node_get_child_by_index(nod, i)) &&
			dico_node_accept_word_size(nod, size)
		)
			eval[i] = dico_node_get_eval(child);
		else
			eval[i] = 0;
	}
}

// affichage

void
dico_node_print(struct DicoNode* nod) {
	if(nod) {
		printf("DicoNode[%p]\n", nod);
		printf("\tword = [%d]\n", nod->is_word);
		printf("\tnum_leaf = %d\n\tflag_size = %d\n", nod->num_leaf, nod->flag_size);
		printf("\teval = %d\n", nod->eval);
		printf("\tchild = [\n");
		for(int i = 0; i < ALPHABET_LENGTH; i++)
			printf("\t\t%c => [%p]\n", i + ALPHA_CHAR, nod->child[i]);
		printf("\t];\n");
	}
	else
		printf("DicoNode[%p]\n", nod);
}
