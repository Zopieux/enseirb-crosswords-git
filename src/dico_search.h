/*
Fonctions qui permettent de faire des recherches dans le dictionnaire.
*/

#ifndef DICO_SEARCH_H_
#define DICO_SEARCH_H_

#include "word_list.h"
#include "const.h"

// Implémentation des noeuds de recherche
#include "dico_node.h"

DICO* dico_search_new_from_word_list(WordList* wl);

void dico_search_delete(DICO* dico);

int dico_search_prefix_size_sort_best(DICO *dico, char *prefix_col, char *prefix_lin, int size_col, int size_lin, char ret_chars[]);

int dico_search_is_word(DICO* dico, char *word);
void dico_search_enable_word(DICO* dico, char* word);
void dico_search_disable_word(DICO* dico, char* word);

#endif
