/*
Structure en arbre pour stocker le dictionnaire. Fonctions de construction de
l'arbre et de recherche préfixe.
*/

#ifndef DICO_NODE_H_
#define DICO_NODE_H_

#include "const.h"

struct DicoNode;
#define DICO struct DicoNode

typedef int EVAL_ARRAY[ALPHABET_LENGTH];

struct DicoNode* dico_node_new();

void dico_node_delete(struct DicoNode* nod);

// accesseurs
int dico_node_get_word_count(struct DicoNode* nod);
void dico_node_add_word_count(struct DicoNode* nod);

int dico_node_accept_word_size(struct DicoNode* nod, int size);
void dico_node_add_word_size(struct DicoNode* nod, int size);

int dico_node_get_eval(struct DicoNode* nod);
void dico_node_set_eval(struct DicoNode* nod, int eval);

int dico_node_is_word(struct DicoNode* nod);
void dico_node_set_word(struct DicoNode* nod);
void dico_node_unset_word(struct DicoNode* nod);

// recherche
struct DicoNode* dico_node_get_child_by_index(struct DicoNode* nod, int index);
struct DicoNode* dico_node_get_child(struct DicoNode* nod, char letter);
struct DicoNode* dico_node_get_child_by_prefix(struct DicoNode* nod, char *prefix);

// construction
void dico_node_add_child(struct DicoNode* nod, char letter);
void dico_node_add_child_word(struct DicoNode* nod, char *word);

////
void dico_node_get_eval_array(struct DicoNode* nod, int size, EVAL_ARRAY eval);

// affichage
void dico_node_print(struct DicoNode* nod);

#endif
