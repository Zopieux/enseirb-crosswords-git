#include "dico_search.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "memory.h"
#include "eval.h"
#include "const.h"
#include "utils.h"

DICO*
dico_search_new_from_word_list(WordList* wl) {
	DICO* ret = NULL;
	char buffer[BUFFER_SIZE];

	if(!wl) {
		return NULL;
	}

	ret = dico_node_new();
	dico_node_set_word(ret);

	for(int i = 0; i < word_list_get_size(wl); i++)
		dico_node_add_child_word(ret, word_list_get(wl, i, buffer, BUFFER_SIZE));

	return ret;
}


void
dico_search_delete(DICO* dico) {
	dico_node_delete(dico);
}

int
dico_search_prefix_size_sort_best(
	DICO *dico,
	char *prefix_col,
	char *prefix_lin,
	int size_col,
	int size_lin,
	char ret_chars[]
) {
#define LINE 0
#define COLUMN 1
	EVAL_ARRAY eval[2];
	DICO* nod[2];
	int size[2] = {size_lin, size_col};

	nod[LINE] = dico_node_get_child_by_prefix(dico, prefix_lin);
	nod[COLUMN] = dico_node_get_child_by_prefix(dico, prefix_col);

	if(!nod[LINE] || !dico_node_accept_word_size(nod[LINE], size_lin))
		return 0;
	if(!nod[COLUMN] || !dico_node_accept_word_size(nod[COLUMN], size_col))
		return 0;

	dico_node_get_eval_array(nod[LINE], size[LINE], eval[LINE]);
	dico_node_get_eval_array(nod[COLUMN], size[COLUMN], eval[COLUMN]);

	op_scal(eval[LINE], eval[COLUMN], eval[0], ALPHABET_LENGTH, min);

	return sort_index_desc_pos(eval[0], ALPHABET_LENGTH, ret_chars, ALPHA_CHAR);
#undef LINE
#undef COLUMN
}

int
dico_search_is_word(DICO* dico, char* word) {
	return dico_node_is_word(dico_node_get_child_by_prefix(dico, word));
}

void
dico_search_enable_word(DICO* dico, char* word) {
	dico_node_set_word(dico_node_get_child_by_prefix(dico, word));
}

void
dico_search_disable_word(DICO* dico, char* word) {
	dico_node_unset_word(dico_node_get_child_by_prefix(dico, word));
}
