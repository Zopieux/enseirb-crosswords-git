#include "stack.h"

#include <string.h>

#include "const.h"
#include "memory.h"
#include "utils.h"

struct Stack {
	size_t elem_size;

	void* data;
	int cur_size;
	int max_size;
};

#define ELEMENT_ADRS(data_array, elem_size, elem_pos) (((char*)data_array) + (elem_size) * (elem_pos))

#define BYTE(data_array, index) ((char*)data_array)[i]


// FONCTION PRIVEES

struct Stack*
stack_resize(struct Stack* stack, int new_size) {
	void* new_data = memory_alloc(stack->elem_size * new_size);

	if(stack->data && new_data) {
		int size_to_copy = stack->elem_size * min(stack->max_size, new_size);
		for(int i = 0 ; i < size_to_copy ; i++) {
			BYTE(new_data, i) = BYTE(stack->data, i);
		}
		memory_free(stack->data);
	}
	else if(stack->data)
		new_data = stack->data;

	stack->data = new_data;
	stack->max_size = new_size;

	return stack;
}

// FONCTIONS PUBLIQUES

struct Stack*
stack_new(size_t elem_size) {
	struct Stack* ret = memory_alloc(sizeof(struct Stack));

	ret->elem_size = elem_size;
	ret->max_size = STACK_INIT_SIZE;
	ret->cur_size = 0;

	ret->data = NULL;

	return stack_resize(ret, STACK_INIT_SIZE);
}

void
stack_delete(struct Stack* stack) {
	memory_free(stack->data);
	memory_free(stack);
}

void*
stack_head(struct Stack* stack) {
	if(0 < stack->cur_size)
		return ELEMENT_ADRS(stack->data, stack->elem_size, stack->cur_size - 1);
	else
		return NULL;
}

struct Stack*
stack_push(struct Stack* stack, void* elem) {
	if(stack->max_size <= stack->cur_size)
		stack_resize(stack, stack->max_size * 2);

	memcpy(ELEMENT_ADRS(stack->data, stack->elem_size, stack->cur_size), elem, stack->elem_size);
	stack->cur_size++;
	return stack;
}

struct Stack*
stack_pop(struct Stack* stack) {
	if(0 < stack->cur_size)
		stack->cur_size--;
	return stack;
}

int
stack_is_empty(struct Stack* stack) {
	return stack->cur_size <= 0;
}

int
stack_is_element(struct Stack* stack, void* elem, CMP_FUNC cmp) {
	int is_in = FALSE;
	int i = 0;
	while(!is_in && i < stack->cur_size) {
		is_in = cmp(ELEMENT_ADRS(stack->data, stack->elem_size, i), elem);
		i++;
	}
	return is_in;
}
