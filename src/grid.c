#include "grid.h"
#include "memory.h"
#include "const.h"

#include <stdio.h>
#include <string.h>

Grid*
grid_new(int width, int height) {
	Grid* ret = memory_alloc(sizeof(Grid));

	ret->width = width;
	ret->height = height;

	ret->data = memory_alloc(sizeof(char)*width*height);

	return ret;
}

void
grid_delete(Grid* g) {
	memory_free(g->data);
	memory_free(g);
}

int
grid_get_width(Grid* g) {
	return g->width;
}

int
grid_get_height(Grid* g) {
	return g->height;
}

int
grid_get_size(Grid *g){
	return g->width*g->height;
}

char
grid_get_by_index(Grid *g, int index){
	if(0 <= index && index < grid_get_size(g))
		return g->data[index];
	else
	return 0; // erreur
}

char
grid_get_by_coord(Grid *g, int i, int j){
	if(0 <= i && i < g->height &&
		0 <= j && j < g->width  )
		return g->data[i * g->width + j];
	else
	return 0; // erreur
}

int
grid_get_word_size_line(Grid *g, int index){

	int cpt=0;

	int i=index/g->width;
	int j=index%g->width;

	while(j>=0 && grid_get_by_coord(g,i,j)!=BLACK_SQUARE){
		cpt=cpt+1;
		j=j-1;
	}

	j=(index%g->width)+1;
	while(j<g->height && grid_get_by_coord(g,i,j)!=BLACK_SQUARE){
		cpt=cpt+1;
		j=j+1;
	}
	return cpt;

}


int
grid_get_word_size_column(Grid *g, int index){

	int cpt=0;
	int i=index/g->width;
	int j=index%g->width;

	while(i >= 0 && grid_get_by_coord(g, i, j) != BLACK_SQUARE) {
		cpt++;
		i--;
	}

	i=(index/g->width)+1;
	while(i<g->width && grid_get_by_coord(g,i,j)!=BLACK_SQUARE){
		cpt=cpt+1;
		i=i+1;
	}
	return cpt;

}

void
grid_get_prefix_line(Grid *g, int index, char *ret){
	int i = index / g->width;
	int j = index % g->width;

	int l = 0;

	while(j > 0 && grid_get_by_coord(g, i, j - 1) != BLACK_SQUARE) {
		l++;
		j--;
	}

	int rl = 0;
	while(l) {
		ret[rl] = grid_get_by_coord(g, i, j);
		l--;
		rl++;
		j++;
	}
	ret[rl] = '\0';
}

void
grid_get_prefix_column(Grid *g, int index, char *ret){
	int i = index / g->width;
	int j = index % g->width;

	int l = 0;

	while(i > 0 && grid_get_by_coord(g, i - 1, j) != BLACK_SQUARE) {
		l++;
		i--;
	}

	int rl = 0;
	while(l) {
		ret[rl] = grid_get_by_coord(g, i, j);
		l--;
		rl++;
		i++;
	}
	ret[rl] = '\0';
}


void
grid_set_by_index(Grid *g, int index, char letter){
	if(0 <= index && index < grid_get_size(g))
		g->data[index] = letter;
}

void
grid_set_by_coord(Grid* g, int i, int j, char letter) {
	if(0 <= i && i < g->height && 0 <= j && j < g->width)
		g->data[i * g->width + j] = letter;
}
