/*
Contient les fonction nécessaires à la lecture et l'écriture dans la grille
de mots croisés.
*/

#ifndef GRID_H_
#define GRID_H_

/*
On utilise un tableau à une dimension, en retenant la largeur et la hauteur
de la grille.
*/
typedef struct Grid Grid;
struct Grid {
	int width;
	int height;
	char *data;
};

/*
Fonction : grid_new
Description : créé une grille de taille n*m
Retour : pointeur vers la grille créée
*/
Grid* grid_new(int width, int height);

/*
Fonction : grid_delete
Description : libère l'emplacement mémoire de la grille
*/
void grid_delete(Grid* g);

int grid_get_width(Grid* g);
int grid_get_height(Grid* g);

/*
Fonction : grid_get_size
Description : retourne la largeur * la hauteur de la grille
*/
int grid_get_size(Grid *g);

/*
Fonction : grid_get_by_index
Description : retourn le caractère qui est à l'indice index de la grille
*/
char grid_get_by_index(Grid *g, int index);

/*
Fonction : grid_get_by_coord
Description : retourn le caractère qui est à la ligne i et à la colone j de la grille
*/
char grid_get_by_coord(Grid *g, int i, int j);

/*
Fonction : grid_get_word_size_*
Description : donne la logueur du mot demandé à la case index en ligne ou en colone
*/
int grid_get_word_size_line(Grid *g, int index);
int grid_get_word_size_column(Grid *g, int index);

/*
Fonction : grid_get_prefix_*
Description : donne une chaine de caractère représentant le préfixe de la case
index sur la ligne ou la colone
*/
void grid_get_prefix_line(Grid *g, int index, char *ret);
void grid_get_prefix_column(Grid *g, int index, char *ret);

/*
Fonction: grid_set_*
Description: remplit la case index avec la lettre donnée
*/
void grid_set_by_index(Grid *g, int index, char letter);
void grid_set_by_coord(Grid *g, int i, int j, char letter);

#endif
