/*
Contient les fonctions de rendu de la grille (affichage)
pour différents formats de sortie et une fonction de lecture
de la grille depuis un fichier texte.
*/

#ifndef GRID_IO_H_
#define GRID_IO_H_

#include <stdio.h>
#include "grid.h"

#include "grid.h"

#define AVAILABLE_GRID_FORMATS_LEN 3
#define AVAILABLE_GRID_FORMATS_STR "ascii, html, latex"
#define AVAILABLE_GRID_FORMATS_DEFAULT "ascii"
static const char* AVAILABLE_GRID_FORMATS[AVAILABLE_GRID_FORMATS_LEN] = {"ascii", "html", "latex"};

/*
Construction d'une grille à partir d'un fichier texte.
Format :
  l x h
  grille
Exemple :
  3
  4
  AZ#
   #
  # #
   #
Arguments :
  nom_fichier
  grille

ATTENTION : faire suivre d'un grid_delete() !
*/
Grid* grid_new_from_buffer(FILE* gfile);
Grid* grid_new_from_filename(char* nom_fichier);

// Rendu basique en ASCII-art.
void grid_write_ascii(Grid* g, FILE* buffer);

// Rendu LaTeX, avec la libriairie tikz
void grid_write_latex(Grid* g, FILE* buffer);

// Rendu HTML.
void grid_write_html(Grid* g, FILE* buffer);

#endif
