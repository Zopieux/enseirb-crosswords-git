#include "grid_io.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "const.h"
#include "grid.h"
#include "memory.h"

// TODO : A SUPPRIMER
#define DIGIT_SIZE 5

////////////////////////////////////
// FONCTIONS AUXILLIAIRES PRIVEES //
////////////////////////////////////

static int
file_get_grid_size(FILE* file, int* width, int *height) {
	char buffer[BUFFER_SIZE];

	////////
	// Test des arguments
	if(!file)
		return ERROR_NULL_FILE;

	if(!width || !height)
		return ERROR_NULL_ARG;

	if(fseek(file, 0, SEEK_SET) != 0)
		return ERROR_FSEEK;
	// Fin test
	////////

	fgets(buffer, BUFFER_SIZE, file);
	*width = atoi(buffer);

	fgets(buffer, BUFFER_SIZE, file);
	*height = atoi(buffer);

	return 0; // tout va bien
}

static int
grid_read(Grid* grid, FILE* file) {
	char cur = 0;
	char buffer[BUFFER_SIZE];
	int i = 0, j = 0, width, height;

	////////
	// Test des arguments
	if(!grid)
		return ERROR_NULL_GRID;

	if(!file)
		return ERROR_NULL_FILE;

	if(fseek(file, 0, SEEK_SET) != 0)
		return ERROR_FSEEK;
	// Fin test
	////////

	fgets(buffer, BUFFER_SIZE, file);
	width = atoi(buffer);

	fgets(buffer, BUFFER_SIZE, file);
	height = atoi(buffer);

	if(grid_get_width(grid) != width && grid_get_height(grid) != height)
		return ERROR_GRID_SIZE; // la grille n'a pas la bonne taille

	while(!feof(file)) {
		cur = fgetc(file);

		if(IS_IN_ALPHABET(cur) || cur == BLACK_SQUARE) {
			// si j est trop grand, la fonction ne fait rien
			grid_set_by_coord(grid, i, j, cur);
			j++;
		}
		else if(cur == '\n') {
			// si on a pas finit la ligne, on la remplie avec des espaces
			if(j < grid_get_width(grid)) {
				for(; j < grid_get_width(grid) ; j++)
					grid_set_by_coord(grid, i, j, ' ');
			}
			i++;
			j = 0;
		}
		else {
			grid_set_by_coord(grid, i, j, ' ');
			j++;
		}
	}

	return 0; // tout va bien !
}

/////////////////////////
// FONCTIONS PUBLIQUES //
/////////////////////////

Grid*
grid_new_from_buffer(FILE* gfile) {
	Grid* grid = NULL;

	if(!gfile) {
		fprintf(stderr, "ERROR: unable to open file (%s)\n", strerror(errno));
		return NULL;
	}

	grid = memory_alloc(sizeof(Grid));

	// aquisition de la taille de la grille
	// et nettoyage si erreur
	if(file_get_grid_size(gfile, &grid->width, &grid->height) < 0) {
		fprintf(stderr, "ERROR: in file_get_grid_size\n");
		memory_free(grid);
		grid = NULL;
	}
	else {
		grid->data = memory_alloc(sizeof(char)*grid_get_size(grid));

		// remplissage de la grille en fonction du fichier
		// et nettoyage si erreur
		if(grid_read(grid, gfile) < 0) {
			fprintf(stderr, "ERROR: in grid_read\n");
			memory_free(grid->data);
			memory_free(grid);
			grid = NULL;
		}
	}

	fclose(gfile);
	return grid;
}

Grid*
grid_new_from_filename(char* fname) {
	return grid_new_from_buffer(fopen(fname, "r"));
}

void
grid_write_ascii(Grid* grid, FILE* buffer) {
	for(int line=0; line < grid->height; line++) {
		for(int col=0; col < grid->width; col++) {
			fputc(grid_get_by_coord(grid, line, col), buffer);
		}
		fputc('\n', buffer);
	}
}

void
grid_write_latex(Grid* grid, FILE* buffer) {
	// brace yourselves, code generation incoming.
	fputs(
"\\documentclass[a4paper,10pt]{article}\n\
\\usepackage[utf8]{inputenc}\n\
\\usepackage[T1]{fontenc}\n\
\\usepackage{lmodern}\n\
\\usepackage[margin=1cm]{geometry}\n\
\\usepackage{tikz}\n\
\\usetikzlibrary{positioning}\n\
\\usepackage{multicol}\n\
\\usepackage{amsmath}\n\
\n\
\\renewcommand{\\familydefault}{\\sfdefault}\n\
\\newcommand{\\mtilde}{\\raise.17ex\\hbox{$\\scriptstyle\\mathtt{\\sim}$}}\n\
\\setlength\\parindent{0pt}\n\
\\pagestyle{empty}\n\
\\begin{document}\n\
\\thispagestyle{empty}\n\
\n\
\\centerline{\\huge Grille de mots croisés}\\medskip\n\
\\centerline{Générée par l'algorithme de\n\
		\\mtilde amacabies, \\mtilde esagardia et \\mtilde ldauphin}\\medskip\n\
\n\
\\vspace*{\\fill}\n\
\\begin{center}\n\
\\scalebox{1.1}{\n\
		\\begin{tikzpicture}[header/.style={color=gray,font=\\Large},\n\
		answer/.style={color=black,font=\\huge}]\n", buffer);

	// tracé de la grille
	fprintf(buffer, "\\draw[black] (0,-%d) grid (%d,0);\n",
		grid->height, grid->width);

	// ajout d'une ligne et d'une colonne de nombres sur les bords
	for(int i = 0; i < grid->width; i++) {
		fprintf(buffer, "\\node[header] at (%d.5, 0.5) {%d};\n",
			i, i + 1
		);
	}
	for(int i = 0; i < grid->height; i++) {
		fprintf(buffer, "\\node[header] at (-0.5, -%d.5) {%d};\n",
			i, i + 1
		);
	}

	// remplissage des cases
	for(int line = 0; line < grid->height; line++) {
		for(int col = 0; col < grid->width; col++) {

			char cell = grid_get_by_coord(grid, line, col);

			if(cell == BLACK_SQUARE) {
				fprintf(buffer, "\\fill[black] (%d,-%d) rectangle (%d,-%d);\n",
					col, line, col + 1, line + 1
				);
			} else {
				fprintf(buffer, "\\node[answer] at (%d.5,-%d.5) {%c};\n",
					col, line, cell
				);
			}
		}
	}

	fputs("\\end{tikzpicture}\n\
	}\n\
	\\end{center}\n\
	\\vspace*{\\fill}\\vspace*{\\fill}\n\
	\\hfill Copyright (c) ENSIERB-MATMECA\n\
	\n\
	\\end{document}", buffer);

}

void
grid_write_html(Grid* grid, FILE* buffer) {
	fputs(
"<!doctype html>\n\
<html>\n\
<head>\n\
  <meta charset='utf-8'>\n\
  <title>Grille de mots croisés</title>\n\
  <meta name='author' content='~ldauphin, ~amacabies, ~esagardia'>\n\
  <style type='text/css'>\n\
    body { font-size: 14px; }\n\
    table, td { border: 1px solid black; }\n\
    td { text-align: center; font-size: 18px; padding: 6px 8px; }\n\
    td.bs { background: #333; }\n\
  </style>\n\
</head>\n\
<body>\n\
<h1>Grille de mots croisés</h1>\n\
<h2>Générée par l'algorithme de ~ldauphin, ~amacabies et ~esagardia</h2>\n\
<table cellpadding='3' cellspacing='0' style='border-collapse: collapse;'><tbody>", buffer);

	for(int line = 0; line < grid->height; line++) {
		fputs("<tr>", buffer);
		for(int col = 0; col < grid->width; col++) {

			char cell = grid_get_by_coord(grid, line, col);

			if(cell == BLACK_SQUARE) {
				fputs("<td class='bs'></td>", buffer);
			} else {
				fprintf(buffer, "<td>%c</td>", cell);
			}
		}
		fputs("</tr>", buffer);
	}

	fputs("</tbody>\n</table>\n</body>\n</html>", buffer);
}
