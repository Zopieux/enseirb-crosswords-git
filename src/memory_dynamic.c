/*

Ce fichier est une implémentation de memory.h, ce qui est aussi le cas de memory_static.c, ces deux fichiers sont donc incompatibles :

==> NE PAS COMPILER AVEC "memory_static.c" <==

 */

#include "memory.h"
#include <stdlib.h>

// DEBUG //
//static int compt = 0;

void
memory_init() {}

void*
memory_alloc(size_t size) {
  // DEBUG //
  //compt++;
  return malloc(size);
}

void
memory_free(void* ptr) {
  free(ptr);
  // DEBUG //
  //compt--;
}

void
memory_quit() {
  // DEBUG //
  // Vérification à la fin du nombre de libérations manquantes
  //printf("%d pointeurs n'ont pas été libérés !\n", compt);
}
